# worthless-launcher

A worthless CLI launcher written in Python.

> For a nice GUI launcher you should check out [An Anime Game Launcher](https://github.com/an-anime-team/an-anime-game-launcher)

Check out its website at https://tretrauit.gitlab.io/worthless-launcher for more information.

> The current branch will enter maintenance mode, for the latest development please check `refactor` branch
