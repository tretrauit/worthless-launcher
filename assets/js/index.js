const themeBtn = document.querySelector("a.button");

function enableDarkMode() {
    DarkReader.enable({
        brightness: 100,
        contrast: 100,
    });
    themeBtn.innerHTML = "☼";
    localStorage.setItem("darkMode", 1);
    console.log(localStorage.darkMode)
}

function disableDarkMode() {
    DarkReader.disable();
    themeBtn.innerHTML = "☾";
    localStorage.setItem("darkMode", 0);
    console.log(localStorage.darkMode)
}

themeBtn.addEventListener("click", function() {
    const isEnabled = DarkReader.isEnabled();
    if (isEnabled) {
        disableDarkMode();
        return;
    }
    enableDarkMode();
})

let darkMode = parseInt(localStorage.darkMode);
if (darkMode === 1) {
    enableDarkMode();
} else if (isNaN(darkMode)) {
    disableDarkMode();
}

// Fix git repo buttons (open a new tab)
for (let i = 2; i < 4; i++) {
    const btn = document.querySelectorAll(`a.button`)[i];
    btn.setAttribute("target", "_blank");
}