import pathlib

from setuptools import setup

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

setup(
    name='worthless',
    version='2.2.22',
    packages=['worthless', 'worthless.classes', 'worthless.classes.launcher', 'worthless.classes.installer'],
    url='https://git.tretrauit.me/tretrauit/worthless-launcher',
    license='MIT License',
    author='tretrauit',
    author_email='tretrauit@gmail.org',
    description='A worthless CLI launcher written in Python.',
    long_description=README,
    long_description_content_type="text/markdown",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3"
    ],
    include_package_data=True,
    install_requires=["aiohttp", "appdirs", "aiopath"]
)
