import unittest
import asyncio
import worthless
from worthless.classes import launcher, installer
game_launcher = worthless.Launcher()
game_installer = worthless.Installer()


class LauncherOverseasTest(unittest.TestCase):
    def test_get_version_info(self):
        version_info = asyncio.run(game_launcher.get_resource_info())
        print("get_resource_info test.")
        print("get_resource_info: ", version_info)
        print("raw: ", version_info.raw)
        self.assertIsInstance(version_info, installer.Resource)

    def test_get_launcher_info(self):
        launcher_info = asyncio.run(game_launcher.get_launcher_info())
        print("get_launcher_info test.")
        print("get_launcher_info: ", launcher_info)
        print("raw: ", launcher_info.raw)
        self.assertIsInstance(launcher_info, launcher.Info)

    def test_get_launcher_full_info(self):
        launcher_info = asyncio.run(game_launcher.get_launcher_full_info())
        print("get_launcher_full_info test.")
        print("get_launcher_full_info: ", launcher_info)
        print("raw: ", launcher_info.raw)
        self.assertIsInstance(launcher_info, launcher.Info)

    def test_get_launcher_background_url(self):
        bg_url = asyncio.run(game_launcher.get_launcher_background_url())
        print("get_launcher_background_url test.")
        print("get_launcher_background_url: ", bg_url)
        self.assertIsInstance(bg_url, str)
        self.assertTrue(bg_url)

    def test_get_installer_game_diff(self):
        game_diff = asyncio.run(game_installer.get_game_diff_archive("2.4.0"))
        print("get_game_diff_archive test.")
        print("get_game_diff_archive: ", game_diff)
        print("raw: ", game_diff.raw)
        self.assertIsInstance(game_diff, installer.Diff)

    def test_get_installer_voiceover_diff_one(self):
        game_diff = asyncio.run(game_installer.get_voiceover_diff_archive("en-us", "2.4.0"))
        print("get_voiceover_diff_archive test one (en-us)")
        print("get_voiceover_diff_archive: ", game_diff)
        print("raw: ", game_diff.raw)
        self.assertIsInstance(game_diff, installer.Voicepack)

    def test_get_installer_voiceover_diff_two(self):
        game_diff = asyncio.run(game_installer.get_voiceover_diff_archive("en-us", "2.4.0"))
        print("get_voiceover_diff_archive test two (English(US))")
        print("get_voiceover_diff_archive: ", game_diff)
        print("raw: ", game_diff.raw)
        self.assertIsInstance(game_diff, installer.Voicepack)

if __name__ == '__main__':
    unittest.main()
