#!/usr/bin/python3
import asyncio
from worthless import cli

if __name__ == "__main__":
    asyncio.run(cli.main())
