from worthless.classes.installer import resource, game, latest, diff, voicepack
Resource = resource.Resource
Game = game.Game
Latest = latest.Latest
Diff = diff.Diff
Voicepack = voicepack.Voicepack
