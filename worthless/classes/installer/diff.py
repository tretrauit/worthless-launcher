from worthless.classes.installer.voicepack import Voicepack


class Diff:
    def __init__(self, name, version, path, size, md5, is_recommended_update, voice_packs, raw):
        self.name = name
        self.version = version
        self.path = path
        self.size = size
        self.md5 = md5
        self.is_recommended_update = is_recommended_update
        self.voice_packs = voice_packs
        self.raw = raw

    def get_name(self):
        return self.path.split("/")[-1]

    @staticmethod
    def from_dict(data):
        voice_packs = []
        for v in data['voice_packs']:
            voice_packs.append(Voicepack.from_dict(v))
        return Diff(data["name"], data["version"], data["path"], data["size"], data["md5"],
                    data["is_recommended_update"], voice_packs, data)
