from worthless.classes.installer.latest import Latest
from worthless.classes.installer.diff import Diff


class Game:
    def __init__(self, latest, diffs, raw):
        self.latest = latest
        self.diffs = diffs
        self.raw = raw

    @staticmethod
    def from_dict(data):
        try:
            diffs = []
            for diff in data['diffs']:
                diffs.append(Diff.from_dict(diff))
            return Game(Latest.from_dict(data['latest']), diffs, data)
        except (KeyError, ValueError, TypeError):
            return data
