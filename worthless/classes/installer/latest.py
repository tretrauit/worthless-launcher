from worthless.classes.installer.voicepack import Voicepack


class Latest:
    def __init__(self, name, version, path, size, md5, entry, voice_packs, decompressed_path, segments, raw):
        self.name = name
        self.version = version
        self.path = path
        self.size = size
        self.md5 = md5
        self.entry = entry
        self.voice_packs = voice_packs
        self.decompressed_path = decompressed_path
        self.segments = segments
        self.raw = raw

    def get_name(self):
        name = self.path.split("/")[-1]
        if name == "":
            name = self.segments[0]["path"].split("/")[-1][:-4]
        return name 

    @staticmethod
    def from_dict(data):
        voice_packs = []
        for v in data['voice_packs']:
            voice_packs.append(Voicepack.from_dict(v))
        return Latest(data["name"], data["version"], data["path"], data["size"], data["md5"], data["entry"],
                      voice_packs, data["decompressed_path"], data["segments"], data)
