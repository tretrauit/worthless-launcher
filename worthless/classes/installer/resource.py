from worthless.classes.installer.game import Game


class Resource:
    """Contains the game resource information.

    Everything except `game` is not wrapped yet

    Attributes:

    - :class:`worthless.classes.launcher.background.Background` background: The launcher background information.
    - :class:`worthless.classes.launcher.banner.Banner` banner: The launcher banner information.
    - :class:`worthless.classes.launcher.iconbutton.IconButton` icon: The launcher icon buttons information.
    - :class:`worthless.classes.launcher.qq.QQ` post: The launcher QQ posts information.
    - :class:`dict` raw: The launcher raw information.
    """
    def __init__(self, game, plugin, web_url, force_update, pre_download_game, deprecated_packages, sdk, raw):
        self.game = game
        self.plugin = plugin
        self.web_url = web_url
        self.force_update = force_update
        self.pre_download_game = pre_download_game
        self.deprecated_packages = deprecated_packages
        self.sdk = sdk
        self.raw = raw

    @staticmethod
    def from_dict(data):
        return Resource(Game.from_dict(data['game']), data['plugin'], data['web_url'], data['force_update'],
                        Game.from_dict(data['pre_download_game']), data['deprecated_packages'], data['sdk'], data)
