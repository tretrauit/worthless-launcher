class Voicepack:
    def __init__(self, language, name, path, size, md5, raw):
        self.language = language
        self.name = name
        self.path = path
        self.size = size
        self.md5 = md5
        self.raw = raw

    def get_name(self):
        return self.path.split("/")[-1]

    @staticmethod
    def from_dict(data):
        return Voicepack(data["language"], data["name"], data["path"], data["size"], data["md5"], data)
