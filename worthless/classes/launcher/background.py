class Background:
    """Contains the launcher background information

    Note that the `background` variable is an url to the background image,
    while the `url` variable contains an empty string, so it seems that the
    `url` and `icon` variables are not used by the official launcher itself.

    Also, the launcher background checksum is using an algorithm which I
    haven't found out yet, so you better not rely on it but instead rely
    on the url name which contains a md5 sum in the first part of the name.

    Attributes:

    - :class:`str` background: The launcher background url.
    - :class:`str` icon: The icon url.
    - :class:`str` url: The url variable.
    - :class:`str` version: The launcher background version.
    - :class:`str` bg_checksum: The launcher background checksum.
    - :class:`dict` raw: The launcher background raw information.
    """

    def __init__(self, background, icon, url, version, bg_checksum, raw):
        """Inits the launcher background class"""
        self.background = background
        self.icon = icon
        self.url = url
        self.version = version
        self.bg_checksum = bg_checksum
        self.raw = raw

    @staticmethod
    def from_dict(data) -> 'Background':
        """Creates a launcher background from a dictionary."""
        return Background(data["background"], data["icon"], data["url"],
                          data["version"], data["bg_checksum"], data)
