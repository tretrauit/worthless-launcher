class Banner:
    """Contains a launcher banner information

    Note that the banner name is in chinese, so you may not find this variable useful.
    Also, the banner has a variable called `order`, you can use that to sort the banner
    like the official launcher does.

    Attributes:

    - :class:`str` banner_id: The launcher banner id.
    - :class:`str` name: The banner name.
    - :class:`str` img: The banner image url.
    - :class:`str` url: The banner target url.
    - :class:`str` order: The banner order.
    - :class:`str` name: The banner name.
    - :class:`dict` raw: The banner raw information.
    """
    def __init__(self, banner_id, name, img, url, order, raw):
        self.banner_id = banner_id
        self.name = name
        self.img = img
        self.url = url
        self.order = order
        self.raw = raw

    @staticmethod
    def from_dict(data) -> 'Banner':
        """Creates a launcher banner from a dictionary."""
        return Banner(data["banner_id"], data["name"], data["img"], data["url"], data["order"], data)
