from worthless.classes.launcher.iconotherlink import IconOtherLink


class IconButton:
    """Contains a launcher icon button information

    The `qr_img`, `qr_desc` variables are not used in the official launcher
    (since it's empty)

    Attributes:

    - :class:`str` icon_id: The icon id.
    - :class:`str` img: The icon url.
    - :class:`str` tittle: The icon title.
    - :class:`str` url: The icon target url.
    - :class:`str` qr_img: The QR code url.
    - :class:`str` qr_desc: The QR code description.
    - :class:`str` img_hover: The icon url when hovered over.
    - :class:`list[LauncherIconOtherLink]` other_links: Other links in the button.
    - :class:`dict` raw: The launcher background raw information.
    """

    def __init__(self, icon_id, img, tittle, url, qr_img, qr_desc, img_hover, other_links, raw):
        """Inits the launcher icon class"""
        self.icon_id = icon_id
        self.img = img
        self.tittle = tittle
        self.url = url
        self.qr_img = qr_img
        self.qr_desc = qr_desc
        self.img_hover = img_hover
        self.other_links = other_links
        self.raw = raw

    @staticmethod
    def from_dict(data) -> 'IconButton':
        """Creates a launcher background from a dictionary."""
        other_links = []
        for link in data['other_links']:
            other_links.append(IconOtherLink.from_dict(link))
        return IconButton(data["icon_id"], data["img"], data["tittle"], data["url"], data["qr_img"],
                          data["qr_desc"], data["img_hover"], other_links, data)
