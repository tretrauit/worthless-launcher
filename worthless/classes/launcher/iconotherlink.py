class IconOtherLink:
    def __init__(self, title, url):
        self.title = title
        self.url = url

    @staticmethod
    def from_dict(data) -> 'IconOtherLink':
        """Creates a launcher icon other link from a dictionary."""
        return IconOtherLink(data["title"], data["url"])
