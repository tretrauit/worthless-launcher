from worthless.classes.launcher import background, banner, iconbutton, post, qq
Background = background.Background
Banner = banner.Banner
IconButton = iconbutton.IconButton
Post = post.Post
QQ = qq.QQ


class Info:
    """Contains the launcher information

    Note that I, myself doesn't use QQ so if you encounter any issues, please open a new issue
    or better a PR to fix that problem.

    Attributes:

    - :class:`worthless.classes.launcher.background.Background` background: The launcher background information.
    - :class:`worthless.classes.launcher.banner.Banner` banner: The launcher banner information.
    - :class:`worthless.classes.launcher.iconbutton.IconButton` icon: The launcher icon buttons information.
    - :class:`worthless.classes.launcher.qq.QQ` post: The launcher QQ posts information.
    - :class:`dict` raw: The launcher raw information.
    """
    def __init__(self, lc_background: Background, lc_banner: list[Banner], icon: list[IconButton], lc_post: list[Post],
                 lc_qq: list[QQ], raw: dict):
        self.background = lc_background
        self.banner = lc_banner
        self.icon = icon
        self.post = lc_post
        self.qq = lc_qq
        self.raw = raw

    @staticmethod
    def from_dict(data):
        bg = Background.from_dict(data["adv"])
        lc_banner = []
        for b in data["banner"]:
            lc_banner.append(Banner.from_dict(b))
        lc_icon = []
        for i in data["icon"]:
            lc_icon.append(IconButton.from_dict(i))
        lc_post = []
        for p in data["post"]:
            lc_post.append(Post.from_dict(p))
        lc_qq = []
        for q in data["qq"]:
            lc_qq.append(QQ.from_dict(q))
        return Info(bg, lc_banner, lc_icon, lc_post, lc_qq, data)

