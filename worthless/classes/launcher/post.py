class Post:
    """Contains a launcher post information

    The `type` variable can be POST_TYPE_ANNOUNCE, POST_TYPE_ACTIVITY and POST_TYPE_INFO
    where announce is an announcement, activity is an activity/event and info is an information.

    The `show_time` variable is the time in DD/MM format when the post will be shown.

    Also, `tittle` is not my typo, and it's the server intention (probably there are clients
    where their developers wrote `tittle` instead of `title`), and the post has a variable
    called `order`, you can use that to sort the post like the official launcher does.

    Attributes:

    - :class:`str` post_id: The launcher post id.
    - :class:`str` type: The post type, can be POST_TYPE_ANNOUNCE, POST_TYPE_ACTIVITY and POST_TYPE_INFO
    - :class:`str` tittle: The post title.
    - :class:`str` url: The post target url.
    - :class:`str` show_time: The time when the post will be shown.
    - :class:`str` order: The post order.
    - :class:`str` title: The post title.
    - :class:`dict` raw: The post raw information.
    """
    def __init__(self, post_id, post_type, tittle, url, show_time, order, title, raw):
        self.post_id = post_id
        self.type = post_type  # Shadow built-in name `type`
        self.tittle = tittle
        self.url = url
        self.show_time = show_time
        self.order = order
        self.title = title
        self.raw = raw

    @staticmethod
    def from_dict(data) -> 'Post':
        """Creates a launcher post from a dictionary."""
        return Post(data["post_id"], data["type"], data["tittle"], data["url"],
                    data["show_time"], data["order"], data["title"], data)
