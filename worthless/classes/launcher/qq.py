class QQ:
    """Contains the launcher QQ information

    Note that I, myself doesn't use QQ so if you encounter any issues, please open a new issue
    or better a PR to fix that problem.

    Attributes:
    - :class:`str` qq_id: The id of the QQ post
    - :class:`str` name: The name of the QQ post
    - :class:`int` number: The number of the QQ post
    - :class:`str` code: The QQ post url.
    - :class:`dict` raw: The launcher raw information.
    """
    def __init__(self, qq_id, name, number, code, raw):
        self.qq_id = qq_id
        self.name = name
        self.number = number
        self.code = code
        self.raw = raw

    @staticmethod
    def from_dict(raw: dict) -> 'QQ':
        """Creates a QQ object from a dictionary

        Args:
            raw (dict): The raw dictionary

        Returns:
            QQ: The QQ object
        """
        qq_id = raw.get('qq_id')
        name = raw.get('name')
        number = int(raw.get('number'))
        code = raw.get('code')
        return QQ(qq_id, name, number, code, raw)
