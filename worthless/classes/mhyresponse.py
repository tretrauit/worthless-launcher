class mhyResponse:
    """Simple class for wrapping miHoYo web response
    Currently not used for anything.
    """
    def __init__(self, retcode, message, data):
        self.retcode = retcode
        self.message = message
        self.data = data
