from appdirs import AppDirs


APP_NAME="worthless"
APP_AUTHOR="tretrauit"
APPDIRS=AppDirs(APP_NAME, APP_AUTHOR)
LAUNCHER_API_URL_OS = "https://sdk-os-static.hoyoverse.com/hk4e_global/mdk/launcher/api"
LAUNCHER_API_URL_CN = "https://sdk-static.mihoyo.com/hk4e_cn/mdk/launcher/api"
HDIFFPATCH_GIT_URL="https://github.com/sisong/HDiffPatch"
PATCH_LIST = {
    "Krock": "https://notabug.org/Krock/dawn",
    "y0soro": "https://notabug.org/y0soro/dawn"
}
TELEMETRY_URL_LIST = [
    "log-upload-os.mihoyo.com",
    "log-upload-eur.mihoyo.com",
    "log-upload-os.hoyoverse.com",
    "overseauspider.yuanshen.com"
]
TELEMETRY_URL_CN_LIST = [
    "log-upload.mihoyo.com",
    "uspider.yuanshen.com"
]
TELEMETRY_OPTIONAL_URL_LIST = [
    "prd-lender.cdp.internal.unity3d.com",
    "thind-prd-knob.data.ie.unity3d.com",
    "thind-gke-usc.prd.data.corp.unity3d.com",
    "cdp.cloud.unity3d.com",
    "remote-config-proxy-prd.uca.cloud.unity3d.com"
]