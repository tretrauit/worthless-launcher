from configparser import ConfigParser
from pathlib import Path

from aiopath import AsyncPath


class LauncherConfig:
    """
    Provides config.ini for official launcher compatibility
    """

    @staticmethod
    def create_config(game_version, overseas=True):
        """
        Creates config.ini
        """
        sub_channel = "0" if overseas else "1"
        config = ConfigParser()
        config.add_section("General")
        config.set("General", "channel", "1")
        config.set("General", "cps", "mihoyo")
        config.set("General", "game_version", game_version)
        config.set("General", "sdk_version", "")
        config.set("General", "sub_channel", sub_channel)
        return config

    def __init__(self, config_path, game_version=None, overseas=True):
        if isinstance(config_path, str | AsyncPath):
            config_path = Path(config_path)
        if not game_version:
            game_version = "0.0.0"
        self.config_path = config_path
        self.config = ConfigParser()
        if self.config_path.exists():
            self.config.read(self.config_path)
        else:
            self.config = self.create_config(game_version, overseas)

    def set_game_version(self, game_version):
        self.config.set("General", "game_version", game_version)

    def set_overseas(self, overseas=True):
        sub_channel = "0" if overseas else "1"
        self.config.set("General", "sub_channel", sub_channel)

    def save(self):
        """
        Saves config.ini
        """
        with self.config_path.open("w") as config_file:
            self.config.write(config_file)
